#Five of a Kind - Manufacturing Optimization Engine

##Project Overview

Optimizing manufacturing process by reducing manufacturing delays, minimising extra inventory and backlogs. We acheived this by making better decisions by measuring and analysing current Sales orders, Backlog orders, predicting delays during production and prediciting future production demand.


##Solution Description

We used the ARIMA model to predict the future demand and devised an Algorithm to optimize the work flow in six production lines with three steps each. Our algorithm tries to optimize the process by allocating a product on a line by finding the line which gets free at the earliest. When the numbers are changed it devises a plan for the optimal utilization of the prodution line and displays it for the next 1 week in the UI.    

##Architectural Diagram

![](./arc_diag.jpeg)

##Technical Description 

We used   * React : "^17.0.1"
		  * Flask
		  * MongoDB
		  * Redux
 
To run the solution these packages need to be installed :

For Front-end :

		* react
		* material-ui
		* react-chartjs-2
		* react-dom
		* react-json-table
		* react-redux
		* react-scripts
		* redux
		* redux-thunk
		* semantic-ui-react
		* web-vitals

For Back-end :

		* flask
		* flask-restful
		* flask-mongoengine
		* flask-bcrypt
		* flask-cors
		* pandas
		* matplotlib
		* scipy
		* statsmodels
		* sklearn
		* numpy

	And requires python version : 3.8

##Team Composition
	1. Pranay Bandaru
		-UI & Front-End
		-Devising and implementing the algorithm
	2. Visvesh Naraharisetty
		-Backend
		-Deploying ML models
	3. Shanmukha Sreenivas
		-Devising and implementing the algorithm
		-Synthesising Data & Building ML Models
	4. Sai Lahar 
		-Synthesising Data
		-Building ML models