from controllers.xps_arima import XPS, VOSTRO, GSERIES, INSPIRON


def initialize_routes(api):

    api.add_resource(XPS, '/api/xps')
    api.add_resource(VOSTRO, '/api/vostro')
    api.add_resource(GSERIES, '/api/gseries')
    api.add_resource(INSPIRON, '/api/inspiron')
