from flask import request, Response, jsonify

from flask_restful import Resource
import datetime
import json
import pandas as pd
import io
import math
import numpy as np
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.stattools import adfuller
from scipy.ndimage.interpolation import shift
from sklearn.metrics import mean_squared_error
from math import sqrt
import matplotlib
import matplotlib.pyplot as plt


class XPS(Resource):
    def get(self):

        train_data = pd.read_csv(
            r'C:\Users\LENOVO\Desktop\CS\Projects\Dell\Manufacturing_Optimization\server\controllers\sales_data_mul.csv')

        train_data.head()
        productData = train_data

        productData.head()

        def difference(dataset, interval=1):
            diff = list()
            for i in range(interval, len(dataset)):
                value = dataset[i] - dataset[i - interval]
                diff.append(value)
            return np.array(diff)

        def inverse_difference(history, yhat, interval=1):
            return yhat + history[-interval]

        def describeSeries(data, label):
            fig = matplotlib.pyplot.gcf()
            fig.set_size_inches(18.5, 10.5)
            plt.plot(data, label="Series")
            plt.plot(pd.rolling_mean(data, window=2),
                     '--', label="Rolling mean")
            plt.plot(pd.rolling_std(data, 2), ":", label="Rolling Std")
            plt.legend()
            plt.savefig(label)
            plt.clf()

        def splitTrainTest(series, testSplit):
            totalData = len(series)
            trainSplit = int(totalData * (1 - testSplit))
            trainSet = series[:trainSplit]
            testSet = series[trainSplit:]
            return trainSet, testSet

        trainSet, testSet = splitTrainTest(productData['xps'], 0.3)

        differencedTrainSet = difference(trainSet, 52) * 1.0

        model = ARIMA(differencedTrainSet, order=(1, 0, 1))
        model_fit = model.fit(disp=0)
        forecast = model_fit.predict(len(differencedTrainSet), len(
            differencedTrainSet) + len(testSet)+6)

        yPrediction_xps = []
        history_xps = list(trainSet)
        for f in forecast:
            yPredict_xps = inverse_difference(history_xps, f, 52)
            yPrediction_xps.append(math.floor(yPredict_xps))
            history_xps.append(yPredict_xps)

        print(yPrediction_xps)
        return jsonify(xps=yPrediction_xps)


class VOSTRO(Resource):
    def get(self):
        train_data = pd.read_csv(
            r'C:\Users\LENOVO\Desktop\CS\Projects\Dell\Manufacturing_Optimization\server\controllers\sales_data_mul.csv')

        train_data.head()
        productData = train_data
        productData.head()

        def difference(dataset, interval=1):
            diff = list()
            for i in range(interval, len(dataset)):
                value = dataset[i] - dataset[i - interval]
                diff.append(value)
            return np.array(diff)

        def inverse_difference(history, yhat, interval=1):
            return yhat + history[-interval]

        def describeSeries(data, label):
            fig = matplotlib.pyplot.gcf()
            fig.set_size_inches(18.5, 10.5)
            plt.plot(data, label="Series")
            plt.plot(pd.rolling_mean(data, window=2),
                     '--', label="Rolling mean")
            plt.plot(pd.rolling_std(data, 2), ":", label="Rolling Std")
            plt.legend()
            plt.savefig(label)
            plt.clf()

        def splitTrainTest(series, testSplit):
            totalData = len(series)
            trainSplit = int(totalData * (1 - testSplit))
            trainSet = series[:trainSplit]
            testSet = series[trainSplit:]
            return trainSet, testSet

        trainSet, testSet = splitTrainTest(productData['vostro'], 0.3)

        differencedTrainSet = difference(trainSet, 52) * 1.0

        model = ARIMA(differencedTrainSet, order=(1, 0, 1))
        model_fit = model.fit(disp=0)
        forecast = model_fit.predict(len(differencedTrainSet), len(
            differencedTrainSet) + len(testSet) + 6)

        yPrediction_vostro = []
        history_vostro = list(trainSet)
        for f in forecast:
            yPredict_vostro = inverse_difference(history_vostro, f, 52)
            yPrediction_vostro.append(math.floor(yPredict_vostro))
            history_vostro.append(yPredict_vostro)
        return jsonify(vostro=yPrediction_vostro)


class GSERIES(Resource):
    def get(self):
        train_data = pd.read_csv(
            r'C:\Users\LENOVO\Desktop\CS\Projects\Dell\Manufacturing_Optimization\server\controllers\sales_data_mul.csv')

        train_data.head()
        productData = train_data
        productData.head()

        def difference(dataset, interval=1):
            diff = list()
            for i in range(interval, len(dataset)):
                value = dataset[i] - dataset[i - interval]
                diff.append(value)
            return np.array(diff)

        def inverse_difference(history, yhat, interval=1):
            return yhat + history[-interval]

        def describeSeries(data, label):
            fig = matplotlib.pyplot.gcf()
            fig.set_size_inches(18.5, 10.5)
            plt.plot(data, label="Series")
            plt.plot(pd.rolling_mean(data, window=2),
                     '--', label="Rolling mean")
            plt.plot(pd.rolling_std(data, 2), ":", label="Rolling Std")
            plt.legend()
            plt.savefig(label)
            plt.clf()

        def splitTrainTest(series, testSplit):
            totalData = len(series)
            trainSplit = int(totalData * (1 - testSplit))
            trainSet = series[:trainSplit]
            testSet = series[trainSplit:]
            return trainSet, testSet

        trainSet, testSet = splitTrainTest(productData['gseries'], 0.3)

        differencedTrainSet = difference(trainSet, 52) * 1.0

        model = ARIMA(differencedTrainSet, order=(1, 0, 1))
        model_fit = model.fit(disp=0)
        forecast = model_fit.predict(len(differencedTrainSet), len(
            differencedTrainSet) + len(testSet)+6)

        yPrediction_gseries = []
        history_gseries = list(trainSet)
        for f in forecast:
            yPredict_gseries = inverse_difference(history_gseries, f, 52)
            yPrediction_gseries.append(math.floor(yPredict_gseries))
            history_gseries.append(yPredict_gseries)
        return jsonify(gseries=yPrediction_gseries)


class INSPIRON(Resource):
    def get(self):
        train_data = pd.read_csv(
            r'C:\Users\LENOVO\Desktop\CS\Projects\Dell\Manufacturing_Optimization\server\controllers\sales_data_mul.csv')

        train_data.head()

        productData = train_data
        productData.head()

        def difference(dataset, interval=1):
            diff = list()
            for i in range(interval, len(dataset)):
                value = dataset[i] - dataset[i - interval]
                diff.append(value)
            return np.array(diff)

        def inverse_difference(history, yhat, interval=1):
            return yhat + history[-interval]

        def describeSeries(data, label):
            fig = matplotlib.pyplot.gcf()
            fig.set_size_inches(18.5, 10.5)
            plt.plot(data, label="Series")
            plt.plot(pd.rolling_mean(data, window=2),
                     '--', label="Rolling mean")
            plt.plot(pd.rolling_std(data, 2), ":", label="Rolling Std")
            plt.legend()
            plt.savefig(label)
            plt.clf()

        def splitTrainTest(series, testSplit):
            totalData = len(series)
            trainSplit = int(totalData * (1 - testSplit))
            trainSet = series[:trainSplit]
            testSet = series[trainSplit:]
            return trainSet, testSet

        trainSet, testSet = splitTrainTest(productData['inspiron'], 0.3)

        differencedTrainSet = difference(trainSet, 52) * 1.0

        model = ARIMA(differencedTrainSet, order=(1, 0, 1))
        model_fit = model.fit(disp=0)
        forecast = model_fit.predict(len(differencedTrainSet), len(
            differencedTrainSet) + len(testSet)+6)

        yPrediction_inspiron = []
        history_inspiron = list(trainSet)
        for f in forecast:
            yPredict_inspiron = inverse_difference(history_inspiron, f, 52)
            yPrediction_inspiron.append(math.floor(yPredict_inspiron))
            history_inspiron.append(yPredict_inspiron)
        return jsonify(inspiron=yPrediction_inspiron)
