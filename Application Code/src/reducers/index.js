import { combineReducers } from "redux";
import   optimizedReducer  from './optimizedReducer'
import  xpsReducer from  './xpsReducer'
import  gseriesReducer from './gseriesReducer'
import inspironReducer from './inspironReducer'
import vostroReducer from './vostroReducer'


export default combineReducers({
    optimized : optimizedReducer, 
    predict_xps : xpsReducer,
    predict_gseries : gseriesReducer,
    predict_inspiron : inspironReducer,
    predict_vostro : vostroReducer 
})