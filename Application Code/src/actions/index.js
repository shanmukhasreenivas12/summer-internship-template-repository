
import web from "../apis/web";

export const fetchOptimised = (optim) => {
    return {
      type: "FETCH_OPTIMIZED",
      payload: {
        optim: optim,
      },
    };
  };


  export const fetchXPS = () => async (dispatch, getState) => {
    const response = await web.get("/xps");
    dispatch({ type: "FETCH_XPS", payload: response.data });
    console.log(response.data);
  };
  
  export const fetchVostro = () => async (dispatch, getState) => {
    const response = await web.get("/vostro");
    dispatch({ type: "FETCH_VOSTRO", payload: response.data });
    console.log(response.data);
  };
  
  export const fetchInspiron = () => async (dispatch, getState) => {
    const response = await web.get("/inspiron");
    dispatch({ type: "FETCH_INSPIRON", payload: response.data });
    console.log(response.data);
  };
  
  export const fetchGseries = () => async (dispatch, getState) => {
    const response = await web.get("/gseries");
    dispatch({ type: "FETCH_GSERIES", payload: response.data });
    console.log(response.data);
  };
  
